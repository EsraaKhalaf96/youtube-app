import React from "react";
import "./App.css";
import SearchBar from "./component/searchBar";
import youtube from "./component/Apis/youtube";
import VideoList from "./component/videoList";
import VideoDetail from "./component/VideoDetails";
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { videos: [], selectedVideo: null };
  }
  componentDidMount(){
    this.onTermSubmit('bulidings')
    // this.setState({ selectedVideo: 'cats'})
  }
  onTermSubmit = async (term) => {
    const response = await youtube.get("/search", {
      params: {
        q: term,
      },
    });
     console.log(response)
    this.setState({ videos: response.data.items, selectedVideo: response.data.items[0]});
    // this.displayVideosList()
  };
  onSelectedVideo = (selectedVideo) => {
    this.setState({ selectedVideo: selectedVideo});
  };
  render() {
    return (
      <div>
      <div className="ui container">
        <SearchBar onTermSubmit={this.onTermSubmit} />
        <div className="ui grid">
          <div className="ui row">
            <div className="eleven wide column">
              <VideoDetail video={this.state.selectedVideo} />
              </div>
              <div className="five wide column">
                {/* the callback function onVideoSelect and state videos is passed to VideoList */}
                <VideoList
                  videos={this.state.videos}
                  onSelectedVideo={this.onSelectedVideo}
                />
              </div>
            </div>
          </div>
        </div>

        {/* <div>
             {
               this.state.videos.map((video) =>
                <li key={video.id.videoId}>
                  {video.snippet.title}
                </li>)
             }
           </div> */}
        {/* {this.displayVideosList} */}
      </div>
    );
  }
}

export default App;
