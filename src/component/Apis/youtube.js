import axios from "axios"

const KEY = 'AIzaSyClIBVKjj3aVt-_EEosvOVUHfVDycbEDyw'
// Axios instance, accessing the YouTube API
export default axios.create({
    baseURL: "https://www.googleapis.com/youtube/v3",
    params: {
      part: "snippet",
      maxResults: 10,
      key: KEY
    }
  });