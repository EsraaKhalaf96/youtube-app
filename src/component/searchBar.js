import React, { Component } from 'react'

export default class SearchBar extends Component {
    state = {term:''}
    onFormSubmit = (e) =>{
       e.preventDefault();
    //    this.setState({term:this.state.term})
       this.props.onTermSubmit(this.state.term)
    }
    render() {
        // console.log(this.state.term)
        return (
            <div className="ui segment">
            <form  className="ui form" onSubmit={this.onFormSubmit}>
                <div className="field">
                <label style= {{textAlign:'left'}}>video Search</label>
                <input type="text" value={this.state.term}  onChange ={(e)=>{this.setState({term:e.target.value})}}/>
                </div>
            </form>
        </div>
        )
    }
}
