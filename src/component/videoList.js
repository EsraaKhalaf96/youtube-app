import React from 'react'
import VideoItem from './VideoItem'

const videoList = ({ videos, onSelectedVideo}) => {
    // map over the passed in videos and create a VideoItem instance for each
    const renderedList = videos.map(video => {
      return (
        <VideoItem
          key={video.id.videoId}
          video={video}
          onSelectedVideo={onSelectedVideo}
        />
      );
    });
    // the list of videos to be displayed is returned here
    return <div className="ui relaxed divided list">{renderedList}</div>;
  };


//another way
//  const videoList = ({videos}) => {
//     return (
//         <div className="ui relaxed divided list">
//              {
//               videos.map((video) =>
//                     <VideoItem  video={video} key={video.id.videoId}/>
//                 )
//              }
//         </div>
//     )
// }
export default videoList
